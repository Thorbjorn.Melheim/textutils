package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if(text.length() > width) {
				throw new IllegalArgumentException("The text is wider than the width of the page");
			}
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			if(text.length() > width) {
				throw new IllegalArgumentException("The text is wider than the width of the page");
			}
			int padding = width - text.length();
			return " ".repeat(padding) + text;
		}

		public String flushLeft(String text, int width) {
			if(text.length() > width) {
				throw new IllegalArgumentException("The text is wider than the width of the page");
			}
			int padding = width - text.length();
			return text + " ".repeat(padding);
		}

		public String justify(String text, int width) {
			if(text.length() > width) {
				throw new IllegalArgumentException("The text is wider than the width of the page");
			}
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("  foo", aligner.flushRight("foo", 5));
	}
	
	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("foo  ", aligner.flushLeft("foo", 5));
	}
	
	@Test
	void testJustify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
}
